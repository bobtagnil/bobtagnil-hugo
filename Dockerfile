FROM klakegg/hugo:ext-debian-onbuild AS hugo

FROM nginx
COPY --from=hugo /target /usr/share/nginx/html
